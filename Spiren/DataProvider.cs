﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Spiren
{
    public class DataProvider 
    {
        private SqlConnection conn; 
        private SqlDataReader reader; 

        public DataProvider()
        {
            conn = new SqlConnection("persist security info=false;integrated security=SSPI;database=Spiren;connect timeout=1");

        }

        public bool AddProduct(string name, double price, int number)
        {
            string insertSql = "INSERT INTO Varer(nummer, navn, pris) VALUES(" + number + ",'" + name + "'," + price + ");"; 
            SqlCommand sqlCmd = new SqlCommand(insertSql, conn);
            conn.Open();
            int rows = sqlCmd.ExecuteNonQuery();
            conn.Close();

            return true;
        }

        public bool UpdateProduct(string name, double price, int number)
        {
            string insertSql = "UPDATE Varer SET navn='" + name + "', pris='" + price + "' WHERE nummer=" + number; 
            SqlCommand sqlCmd = new SqlCommand(insertSql, conn);
            conn.Open();
            int rows = sqlCmd.ExecuteNonQuery();
            conn.Close();

            return true;
        }

        public bool DeleteProduct(int number)
        {
            string insertSql = "DELETE FROM Varer WHERE nummer = " + number; 
            SqlCommand sqlCmd = new SqlCommand(insertSql, conn);
            conn.Open();
            int rows = sqlCmd.ExecuteNonQuery();
            conn.Close();

            return true;
        }

        public List<Db_Vare> FindAllProducts()
        {
            List<Db_Vare> varer = new List<Db_Vare>(); 
            string insertSql = "SELECT * FROM Varer"; 
            SqlCommand sqlCmd = new SqlCommand(insertSql, conn); 
            conn.Open(); 
            reader = sqlCmd.ExecuteReader();
               
	        while (reader.Read()) 
            {
                string name = (string)reader["navn"]; 
                double price = (double)reader["pris"]; 
                int number = (int)reader["nummer"]; 

                varer.Add(new Db_Vare(name, price, number)); 
            } 
            conn.Close();

            return varer; 
        }

        public Db_Vare FindProduct(int productNumber)
        {
            Db_Vare vare = null; 
            string insertSql = "SELECT * FROM Varer WHERE nummer=" + productNumber; 
            SqlCommand sqlCmd = new SqlCommand(insertSql, conn); 
            conn.Open(); 
            reader = sqlCmd.ExecuteReader(); 

            while (reader.Read()) 
            {
                string name = (string)reader["navn"]; 
                double price = (double)reader["pris"]; 
                int number = (int)reader["nummer"]; 

               vare = new Db_Vare(name, price, number); 
            }
            conn.Close();

            return vare; 
        }

        public void PriceIncrease()
        {
            List<Db_Vare> products = FindAllProducts(); 
            List<string> sqlQueries = new List<string>(); 

            foreach (var product in products)
            {
                double newPrice = product.price; 
                double increase = 1; 
                bool update = false; 

                if (product.number > 99999 && product.number < 400000)
                {
                    increase = 1.05; 
                }
                else if ((product.number > 400000 && product.number < 499999) || (product.number > 699999 && product.number < 800000))
                {
                    increase = 1.04; 
                }
                else if (product.number < 100000 || product.number > 799999)
                {
                    increase = 1.03; 
                }

                if (product.price * increase >= 3) 
                {
                    newPrice = product.price * increase; 
                    update = true; 
                }

                if (update) { 
                    sqlQueries.Add("UPDATE Varer SET pris='" + newPrice.ToString().Replace(",", ".") + "' WHERE nummer=" + product.number);
                    
                }
            }

            conn.Open(); 

            foreach (var sqlQuery in sqlQueries) 
            {
                SqlCommand sqlCmd = new SqlCommand(sqlQuery, conn); 
                sqlCmd.ExecuteNonQuery(); 
            }
            conn.Close(); 
        }
    }
}
