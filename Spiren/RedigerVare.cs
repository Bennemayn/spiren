﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spiren
{
    public partial class RedigerVare : Form
    {
        private DataProvider dataProvider; 
        private int number; 
        public RedigerVare(int number)
        {
            InitializeComponent();
            dataProvider = new DataProvider(); 
            this.number = number;

            Db_Vare vare = dataProvider.FindProduct(number); 

            txtBox_name.Text = vare.name; 
            txtBox_price.Text = Convert.ToString(vare.price); 
            txtBox_number.Text = Convert.ToString(vare.number); 
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            dataProvider.DeleteProduct(this.number);
            this.Close();
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            string name = txtBox_name.Text; 
            double price = double.Parse(txtBox_price.Text); 
            dataProvider.UpdateProduct(name, price, number); 
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.google.dk");
        }

    }
}
