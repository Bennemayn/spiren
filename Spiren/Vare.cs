﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spiren
{
    public partial class Vare : Form
    {
        private DataProvider dataProvider;

        public Vare()
        {
            InitializeComponent();
            dataProvider = new DataProvider(); 
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            string name = txtBox_name.Text; 
            double price = double.Parse(txtbox_price.Text); 
            int number = int.Parse(txtBox_number.Text); 

            bool added = dataProvider.AddProduct(name, price, number); 

            if (added)
                this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.google.dk");
        }
    }
}
