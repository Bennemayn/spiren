﻿namespace Spiren
{
    partial class Spiren
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_addproduct = new System.Windows.Forms.Button();
            this.list_products = new System.Windows.Forms.ListView();
            this.button_editproduct = new System.Windows.Forms.Button();
            this.button_updateproducts = new System.Windows.Forms.Button();
            this.button_updateprices = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.SuspendLayout();
            // 
            // button_addproduct
            // 
            this.button_addproduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_addproduct.Location = new System.Drawing.Point(12, 12);
            this.button_addproduct.Name = "button_addproduct";
            this.button_addproduct.Size = new System.Drawing.Size(135, 94);
            this.button_addproduct.TabIndex = 0;
            this.button_addproduct.Text = "Tilføj Vare";
            this.button_addproduct.UseVisualStyleBackColor = true;
            this.button_addproduct.Click += new System.EventHandler(this.button_addproduct_Click);
            // 
            // list_products
            // 
            this.list_products.Location = new System.Drawing.Point(12, 138);
            this.list_products.Name = "list_products";
            this.list_products.Size = new System.Drawing.Size(596, 320);
            this.list_products.TabIndex = 1;
            this.list_products.UseCompatibleStateImageBehavior = false;
            this.list_products.View = System.Windows.Forms.View.Details;
            // 
            // button_editproduct
            // 
            this.button_editproduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_editproduct.Location = new System.Drawing.Point(167, 12);
            this.button_editproduct.Name = "button_editproduct";
            this.button_editproduct.Size = new System.Drawing.Size(135, 94);
            this.button_editproduct.TabIndex = 2;
            this.button_editproduct.Text = "Rediger Vare";
            this.button_editproduct.UseVisualStyleBackColor = true;
            this.button_editproduct.Click += new System.EventHandler(this.button_editproduct_Click);
            // 
            // button_updateproducts
            // 
            this.button_updateproducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_updateproducts.Location = new System.Drawing.Point(319, 12);
            this.button_updateproducts.Name = "button_updateproducts";
            this.button_updateproducts.Size = new System.Drawing.Size(135, 94);
            this.button_updateproducts.TabIndex = 3;
            this.button_updateproducts.Text = "Opdater Varer";
            this.button_updateproducts.UseVisualStyleBackColor = true;
            this.button_updateproducts.Click += new System.EventHandler(this.button_updateproducts_Click);
            // 
            // button_updateprices
            // 
            this.button_updateprices.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_updateprices.Location = new System.Drawing.Point(473, 12);
            this.button_updateprices.Name = "button_updateprices";
            this.button_updateprices.Size = new System.Drawing.Size(135, 94);
            this.button_updateprices.TabIndex = 4;
            this.button_updateprices.Text = "Pris Forhøjelse";
            this.button_updateprices.UseVisualStyleBackColor = true;
            this.button_updateprices.Click += new System.EventHandler(this.button_updateprices_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(597, 493);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(29, 13);
            this.linkLabel1.TabIndex = 5;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Help";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Spiren
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 515);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.button_updateprices);
            this.Controls.Add(this.button_updateproducts);
            this.Controls.Add(this.button_editproduct);
            this.Controls.Add(this.list_products);
            this.Controls.Add(this.button_addproduct);
            this.Name = "Spiren";
            this.Text = "Spiren";
            this.Load += new System.EventHandler(this.Spiren_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_addproduct;
        private System.Windows.Forms.ListView list_products;
        private System.Windows.Forms.Button button_editproduct;
        private System.Windows.Forms.Button button_updateproducts;
        private System.Windows.Forms.Button button_updateprices;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.ColorDialog colorDialog1;
    }
}

