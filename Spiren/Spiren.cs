﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spiren
{
    public partial class Spiren : Form
    {
        private DataProvider dataProvider;  

        public Spiren()
        {
            InitializeComponent();
            dataProvider = new DataProvider(); 
            LoadItemsIntoList();
        }

        private void button_addproduct_Click(object sender, EventArgs e)
        {
            Vare form = new Vare(); 
            form.Show();
        }

        private void button_editproduct_Click(object sender, EventArgs e)
        {
            if (list_products.SelectedItems.Count > 0) {
                int number = int.Parse(list_products.SelectedItems[0].Text); 

                RedigerVare form = new RedigerVare(number); 
                form.Show(); 
            }
        }

        public void LoadItemsIntoList()
        {
            list_products.Clear();
            List<Db_Vare> products = dataProvider.FindAllProducts(); 
            list_products.Columns.Add("Nummer"); 
            list_products.Columns.Add("Navn"); 
            list_products.Columns.Add("Pris"); 

            foreach (var product in products) 
            {
                ListViewItem item = new ListViewItem(Convert.ToString(product.number)); 
                item.SubItems.Add(product.name); 
                item.SubItems.Add(Convert.ToString(product.price)); 

                list_products.Items.Add(item);
            }
        }

        private void button_updateproducts_Click(object sender, EventArgs e)
        {
            LoadItemsIntoList();
        }

      

        private void button_updateprices_Click(object sender, EventArgs e)
        {
            dataProvider.PriceIncrease();


            LoadItemsIntoList();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.google.dk");
        }

        private void Spiren_Load(object sender, EventArgs e)
        {

        }
    }
}
