use spiren

create table Kunder
(
nummer int primary key,
navn varchar(30),
adresse varchar(30),
tlfnr int,
email varchar (30)
)

create table Ordrer
(
nummer int primary key,
dato date,
totalPris float
)

create table Varer
(
nummer int primary key,
navn varchar(30), 
pris float
)

create table OrdreLinier
(
ordreNr int foreign key references Ordrer(nummer),
vareNr int foreign key references Varer(nummer),
kundeNr int foreign key references Kunder(nummer),
antal int,
pris float
)